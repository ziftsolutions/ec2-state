Role Name
=========
ec2-state

Requirements
------------
User must have AWS credentials

Role Variables
--------------
instance_id: The instance id to start, stop, or restart

Dependencies
------------
N/A

Example Playbook
----------------
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: localhost
      become: no
      gather_facts: no
      roles:
         - ec2-state

    $ ansible-playbook -i "localhost," ec2-state \
                       -e "instance_id=i-123456789 \
                          "command=atart"
    $ ansible-playbook -i "localhost," ec2-state \
                       -e "instance_id=i-123456789 \
                          "command=stop"
    $ ansible-playbook -i "localhost," ec2-state \
                       -e "instance_id=i-123456789 \
                          "command=reatart"

License
-------
Zift Solutions, Inc.

Author Information
------------------
Chris Fouts
